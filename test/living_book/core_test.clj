(ns living-book.core-test
  (:require [clojure.test :refer :all]
            [living-book.ch4 :refer :all]))

(deftest a-test
  (testing "If who-are-you return string: Long: 4"
    (is (= "Long: 4" (who-are-you 4)))))
