(ns living-book.ch3)
;; CH 3 - State and Concurency

;; 3.1
(def who-atom (atom :caterpillar))

(defn change [state]
  (case state
    :caterpillar :chrysalis
    :chrysalis :butterfly
    :butterfly))

(def counter (atom 0))

(defn run-5-times [counter]
  (dotimes [_ 5] (swap! counter inc)))

(defn start-3-threads [counter m fun]
  (let [n m]
    (future (dotimes [_ n] (swap! counter fun)))
    (future (dotimes [_ n] (swap! counter fun)))
    (future (dotimes [_ n] (swap! counter fun)))))

(defn start-3-thread-inc [counter]
  (start-3-threads counter 5 inc))

(defn inc-print [val]
  (println val)
  (inc val))

(defn start3-inc-print [counter]
  (start-3-threads counter 2 inc-print))

;; 3.2
(def alice-height (ref 3))
(def right-hand-bites (ref 10))

(defn eat-from-right-hand []
  (dosync (when (pos? @right-hand-bites)
            (alter right-hand-bites dec)
            (alter alice-height #(+ % 24)))))

(defn eat-from-right-hand-commute []
  (dosync (when (pos? @right-hand-bites)
            (commute right-hand-bites dec)
            (commute alice-height #(+ % 24)))))

(defn eat-2-times []
  (let [n 2]
    (future (dotimes [_ 2] (eat-from-right-hand-commute)))
    (future (dotimes [_ 2] (eat-from-right-hand-commute)))
    (future (dotimes [_ 2] (eat-from-right-hand-commute)))))

;; 3.3

(def who-agent (agent :caterpillar))

;; (send who-agent change)
;; (send-off who-agent change)

(defn change-error [state]
  (throw (Exception. "Boom!")))

;; (restart-agent who-agent :caterpillar)
