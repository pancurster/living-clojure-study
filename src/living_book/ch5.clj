(ns living-book.ch5
  (:require [camel-snake-kebab.core :as csk]))

;; CH 5.4
(csk/->snake_case "hello pigeon")

(defn serpent-talk [input]
  (csk/->snake_case input))

(defn -main [& args]
  (println (serpent-talk (first args))))
