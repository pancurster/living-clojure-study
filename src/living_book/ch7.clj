(ns living-book.ch7
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            ;;[cheshire.core :as json]))
            [ring.middleware.json :as ring-json]
            [ring.util.response :as rr]))

;; 7.1, 7.2

(defroutes app-routes
  (GET "/" [] "Hello world")
  (GET "/cheshire-cat" []
       (rr/response {:name "Cheshire Cat"
                     :id 1
                     :state "sleep"}))
  (route/not-found "Not found"))

(def app
  (-> app-routes
      (ring-json/wrap-json-response)
      (wrap-defaults site-defaults)))

