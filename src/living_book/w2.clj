(ns living-book.w2
  (:require [clojure.string :as str]))

;; d1
;; p1
(defn fib [n]
  (cond
    (= n 0) []
    (= n 1) [1]
    :else (loop [c (- n 2)
                 xs [1 1]]
            (if (zero? c) xs
                (recur (dec c)
                       (conj xs (+ (last xs)
                                   (last (butlast xs)))))))))

;;(= fib(n) (+ fib(- n 2) (fib(- n 1))))
;;(= fib(0) '())
;;(= fib(1) '(1))

;; p2
(= ((fn [in]
      (str/join
        (re-seq #"[A-Z]" in))) "HeLlO, WoRlD!")
   "HLOWRD")

;; adereth's solution:
;; (fn [s] (reduce str (filter #(Character/isUpperCase %) s)))

;; factorial fun
(defn fact [x]
  (loop [acc 1, n x]
    (if (zero? n) acc
        (recur (* acc n) (dec n)))))

;; intro to destructuring
(= [2 4] (let [[a b c d e f g] (range)] [c e]))

;; w2d2
(= [1 2 [3 4 5] [1 2 3 4 5]] (let [[a b & c :as d] [1 2 3 4 5]] [a b c d]))

;; A Half-Truth
(defn halftruth [& xs]
  (and ((complement not-any?) true? xs)
       ((complement not-any?) false? xs)))
;; better?:
;;(fn [& xs]
;;  (and (not-every? true? xs) (not-every? false? xs)))

;; Greatest Common Divisor
(defn gcd [a b]
  (if (zero? b) a
      (recur b (rem a b))))
