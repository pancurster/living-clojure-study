(ns living-book.ch4
  (:import (java.net InetAddress)))

;; 4.1
(. "caterpillar" toUpperCase)

(.indexOf "caterpilar" "pilar")

(InetAddress/getByName "localhost")

;; run getHostName on object
(.getHostName (InetAddress/getByName "localhost"))

;; without importing
(java.net.InetAddress/getByName "localhost")

(java.util.UUID/randomUUID)

;; Java	                               | Clojure
;; ------------------------------------------------------------------------
;; "caterpillar".toUpperCase();        | (.toUpperCase "caterpillar")
;; "caterpillar".indexOf("pillar");    | (.indexOf "caterpillar" "pillar")
;; new String("Hi!!");                 | (new String "Hi!!")
;; new String("Hi!!");                 | (String. "Hi!!")
;; InetAddress.getByName("localhost"); | (InetAddress/getByName "localhost")
;; host.getHostName();                 | (.getHostName host)

;; 4.2
(defmulti who-are-you class)

(defmethod who-are-you java.lang.String [in]
  (str "String: " in))
(defmethod who-are-you clojure.lang.Keyword [in]
  (str "Keyword: " in))
(defmethod who-are-you java.lang.Long [in]
  (str "Long: " in))
(defmethod who-are-you :default [in]
  (str "No type for: " in))


;; trochę jak pattern maching?
(defmulti eat (fn [h]
                (if (< h 3)
                  :grow
                  :shrink)))
(defmethod eat :grow [_]
  (str "Eat something"))
(defmethod eat :shrink [_]
  (str "Stop eating so much"))

;; jakies protokoly? - trochę takie interejsy
(defprotocol BigMushroom
  (eat-mushroom [this]))

(extend-protocol BigMushroom
  java.lang.String
  (eat-mushroom [this]
    (str (.toUpperCase this) " mmm tasty"))

  clojure.lang.Keyword
  (eat-mushroom [this]
    (case this
      :grow "Eat more"
      :shring "Eat less"))

  java.lang.Long
  (eat-mushroom [this]
    (if (< this 3)
      (str "Eat more than " this " moshrooms")
      (str "Eat less than " this " moshrooms"))))

(deftype Borowik []
  BigMushroom
  (eat-mushroom [this]
    (str "Chaps, Borowika")))
(deftype Muchomor []
  BigMushroom
  (eat-mushroom [this]
    (str "Chaps, Muchomora i śmierć")))

(def grzyb1 (Borowik.))
(def grzyb2 (Muchomor.))

;; jest jeszcze `defrecord'
