(ns living-book.ch8)

;; 8.1
(when (= 2 2) (println "its true") 33)

(macroexpand-1 '(when (= 2 2) (println "its true")))

;; 8.2
(defmacro def-hi-queen [name phrase]
  (list 'defn
        name
        []
        (list 'hi-queen phrase)))

(defn hi-queen [phrase]
  (str phrase ", so please your Majesty."))

(def-hi-queen jasio "Jestem Jasio")
(def-hi-queen marysia "Jestem pusta Marysia")

;; 8.3
(defmacro def-hi-q [name phrase]
  `(defn ~(symbol name) []
     (hi-queen ~phrase)))

(def-hi-q "Jojo" "Jasiek")
