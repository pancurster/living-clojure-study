(ns living-book.ch2)
;; CH 2 - Flow and Functional Transformations

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(def adjs ["normal"
           "too small"
           "too big"
           "is swimming"])

(defn alice-is [in out]
  (if (empty? in)
    out
    (alice-is (rest in)
              (conj out
                    (str "Alice is " (first in))))))

;; Jak to napisac? Podajemy lylko argument wejsciowy ale mamy
;; rekursję z lambda z 2 parameterami.
;; TODO stackoverflow
(defn alice-is2 [in]
  (let [inner (fn [in out]
      (if (empty? in)
        out
        ((alice-is2 in)
         (rest in)
         (conj out
               (str "Alice have bein " (first in))))))]
    (inner in [])))

;; Byłem sceptyczny co do tego 'loop' ale nie po namyśle, nie jest takie złe.
(defn alice-is3 [input]
  (loop [in input
         out []]
    (if (empty? in)
      out
      (recur (rest in)
             (conj out
                   (str "Alice is loop and " (first in)))))))
