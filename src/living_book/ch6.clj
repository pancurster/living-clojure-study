(ns living-book.ch6
  (:gen-class)
  (:require [clojure.core.async :as async]))

;; 6.1

(def tea-channel (async/chan 10))

(async/>!! tea-channel :cup1)
(async/>!! tea-channel :cup2)
(async/>!! tea-channel :cup3)

;;(async/close! tea-channel)

(async/<!! tea-channel)

(defn goasync []
  (let [tea-chan (async/chan 5)]
    (async/go (async/>! tea-chan :cup-of-tea))
    (async/go (println "Thank you for" (async/<! tea-chan)))))

(defn wait-for-tea [tea-chan]
  (async/go-loop []
    (println "Thank you for the " (async/<! tea-chan))
    (recur)))

(def milk-channel (async/chan 10))
(def sugar-channel (async/chan 10))

(defn wait-for-tea-milk-sugar []
  (async/go-loop []
    (let [[v ch] (async/alts! [tea-channel
                               milk-channel
                               sugar-channel])]
      (println "Got " v " from the " ch)
      (recur))))

;; 6.2

(def google-service-chan (async/chan 10))
(def yahoo-service-chan (async/chan 10))

(defn random-add []
  (reduce + (conj [] (repeat 1 (rand-int 100000)))))

(defn request-google-service []
  (async/go
    (let [[t] (random-add)]
    (async/>! google-service-chan (str t " from google")))))

(defn request-yahoo-service []
  (async/go
    (let [[t] (random-add)]
      (async/>! yahoo-service-chan (str t " from yahoo")))))

;; 6.3
;; lein uberjar - kompilacja do jar
(def result-chan (async/chan 10))

(defn send-request[]
  (request-google-service)
  (request-yahoo-service)
  (async/go (let [[resp] (async/alts!
                          [google-service-chan
                           yahoo-service-chan])]
              (async/>! result-chan resp))))

(defn -main [& args]
  (send-request)
  (println (async/<!! result-chan)))
