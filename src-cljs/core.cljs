(ns cheshire-cat.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [clojure.browser.repl :as repl]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [enfocus.core :as ef]
            [enfocus.events :as ev]
            [enfocus.effects :as ee]))

;; 7.4
;; lein cjlsbuild auto
;; lein ring server
;; lein trampoline cljsbuild repl-listen
;; https://www.niwi.nz/cljs-workshop/
;; 7.5
;; 7.6 enfocus - dom manipulation library
(defn ^:export init[]
  ;;(repl/connect "http://localhost:9000/repl")
  (go
    (let [response (<! (http/get "/cheshire-cat"))
          body (:body response)]
      (ef/at "#cat-name" (ef/content (:name body))
             "#status" (ef/do->
                        (ef/content (:state body))
                        (ef/set-style :font-size "500%")))
      (ef/at "#button1" (ev/listen :click say-goodbye)))))

;; 7.7
(defn say-goodbye []
  (ef/at
   "#cat-name" (ee/fade-out 500)
   "#status" (ee/fade-out 5000)
   "#button1" (ee/fade-out 500)))
