(defproject living_book "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [camel-snake-kebab "0.4.0"]
                 [org.clojure/core.async "0.2.395"]
                 [compojure "1.6.0-beta3"]
                 [ring/ring-defaults "0.2.3"]
                 ;;[cheshire "5.7.0"]]
                 [ring/ring-json "0.5.0-beta1"]
                 [org.clojure/clojurescript "1.9.494"]
                 [cljs-http "0.1.42"]
                 [enfocus "2.1.1"]]
  :plugins [[lein-ring "0.11.0"]
            [lein-cljsbuild "1.1.5"]]
  :ring {:handler living-book.ch7/app}
  :profiles
  {:def {:dependencies [[javax.servlet/servlet-api "2.5"]]}}
  :cljsbuild {
              :builds [{
                        :source-paths ["src-cljs"]
                        :compiler {
                                   :output-to "resources/public/main.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]}
  :main living-book.ch6
  :aot [living-book.ch6])
